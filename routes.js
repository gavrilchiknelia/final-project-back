const getSkills = require('./routes/Skills/getSkills');
const getSkillsId = require('./routes/Skills/getSkillId');
const putSkill = require('./routes/Skills/putSkill');
const postSkill = require('./routes/Skills/postSkill');
const deleteSkill = require('./routes/Skills/deleteSkill');

const postVacancy = require('./routes/Vacancies/postVacancy');
const getVacancies = require('./routes/Vacancies/getVacancies');
const putVacancy = require('./routes/Vacancies/putVacancy');
const getVacancyId = require('./routes/Vacancies/getVacancyId');
const deleteVacancy = require('./routes/Vacancies/deleteVacancy');

const postSphere = require('./routes/Spheres/postSphere');
const getSpheres = require('./routes/Spheres/getSphere');
const putSphere = require('./routes/Spheres/putSphere');
const getSphereId = require('./routes/Spheres/getSphereId');
const deleteSphere = require('./routes/Spheres/deleteSphere');

const postCategory = require('./routes/Categories/postCategory');
const getCategories = require('./routes/Categories/getCategories');
const putCategory = require('./routes/Categories/putCategory');
const getCategoryId = require('./routes/Categories/getCategoryId');
const deleteCategory = require('./routes/Categories/deleteCategory');

const deleteProfession = require('./routes/Professions/deleteProfession');
const getProfessions = require('./routes/Professions/getProfessions');
const getProfessionId = require('./routes/Professions/getProfessionId');
const postProfession = require('./routes/Professions/postProfession');
const putProfession = require('./routes/Professions/putProfession');

module.exports = (app) => {
    deleteProfession(app);
    getProfessions(app);
    getProfessionId(app);
    postProfession(app);
    putProfession(app);

    getSkills(app);
    getSkillsId(app);
    putSkill(app);
    postSkill(app);
    deleteSkill(app);

    postVacancy(app);
    getVacancies(app);
    putVacancy(app);
    getVacancyId(app);
    deleteVacancy(app);

    postSphere(app);
    getSpheres(app);
    putSphere(app);
    getSphereId(app);
    deleteSphere(app);

    postCategory(app);
    getCategories(app);
    putCategory(app);
    getCategoryId(app);
    deleteCategory(app);
};