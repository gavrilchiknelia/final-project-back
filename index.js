const mongoose = require("mongoose");
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const routes = require('./routes');
const mongoURI = require('./config/mongoDB_URL');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.set('port', process.env.PORT || 4000);

mongoose.connect(mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
});

routes(app);

app.listen(app.get('port'), function () {
    console.log("App is listened on 4000 port");
});

