const Vacancy = require("../../models/vacancyModel");

module.exports = (app) => {
    app.post('/vacancies', (req, res) => {
        const vacancy = new Vacancy({
            vacancyName: req.body.vacancyName,
            professionId: req.body.professionId,
        });
        vacancy.save((err, data) => {
            if (err) res.send({
                status: "error",
                message: "Не удалось сохранить вакансию"
            });
            
            else res.send({
                status: "Success",
                result: data,
            });
        });
    });
};
