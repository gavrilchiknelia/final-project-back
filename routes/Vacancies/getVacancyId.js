const Vacancy = require("../../models/vacancyModel");

module.exports = (app) => {
    app.get('/vacancies/:id', (req, res) => {
        Vacancy.findById(req.params.id, (err, oneVacancy) => {
            if (err) res.send({
                status: "error",
                message: 'Не удаллось получить вакансию'
            });
            
            else res.send({
                status: "Success",
                result: oneVacancy,
            });
        });
    });
};