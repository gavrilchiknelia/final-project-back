const Vacancy = require("../../models/vacancyModel");

module.exports = (app) => {
    app.put('/vacancies/:id', (req, res) => {
        Vacancy.findByIdAndUpdate(req.params.id, req.body, (err, updateVacancy) => {
            if (err) res.send({
                status: "error",
                message: 'Не удаллось изменить вакансию'
            });
            
            else res.send({
                status: "Success",
                result: updateVacancy,
            });
        });
    });
};