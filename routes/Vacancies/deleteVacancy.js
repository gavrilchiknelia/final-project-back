const Vacancy = require("../../models/vacancyModel");

module.exports = (app) => {
    app.delete('/vacancies/:id', (req, res) => {
        Vacancy.findByIdAndDelete(req.params.id, (err) => {
            if (err) res.send({status: "error"});
            
            else res.send({
                status: "Success",
            });
        });
    });
};