const Vacancy = require("../../models/vacancyModel");

module.exports = (app) => {
    app.get('/vacancies', (req, res) => {
        Vacancy.find({}, (err, allVacancies) => {
            if (err) res.send({
                status: "error",
                message: 'Не удалось почулить все вакансии'
            });

            else res.send({status: "Success", result: allVacancies});
        });
    });
};